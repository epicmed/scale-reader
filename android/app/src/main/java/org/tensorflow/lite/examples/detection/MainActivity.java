package org.tensorflow.lite.examples.detection;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Mat;
import org.tensorflow.lite.codelabs.digitclassifier.DigitClassifier;
import org.tensorflow.lite.examples.detection.EAST.East;
import org.tensorflow.lite.examples.detection.customview.OverlayView;
import org.tensorflow.lite.examples.detection.env.ImageUtils;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.tflite.Classifier;
import org.tensorflow.lite.examples.detection.tflite.YoloV4Classifier;
import org.tensorflow.lite.examples.detection.tracking.MultiBoxTracker;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.85f;
    private String TAG = "MainActivity";

    static {
        if (!OpenCVLoader.initDebug())
            Log.d("ERROR", "Unable to load OpenCV");
        else
            Log.d("SUCCESS", "OpenCV loaded");
    }

    public Bitmap getBitmapFromAssets(String fileName) throws IOException {
        AssetManager assetManager = getAssets();
        InputStream istr = assetManager.open(fileName);
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        istr.close();

        return bitmap;
    }

    public Bitmap getPbFromAssets(String fileName) throws IOException {
        AssetManager assetManager = getAssets();
        InputStream istr = assetManager.open(fileName);
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        istr.close();

        return bitmap;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        weight_button = findViewById(R.id.weight_button);
        weight_button.setOnClickListener(v -> {
            Intent intent1 = new Intent(MainActivity.this, DetectorActivity.class);
            intent1.putExtra("type", "weight");
            startActivity(intent1);
        });

        temperature_button = findViewById(R.id.temperature_button);
        temperature_button.setOnClickListener(v -> {
            Intent intent1 = new Intent(MainActivity.this, DetectorActivity.class);
            intent1.putExtra("type", "temperature");
            startActivity(intent1);
        });

        temperature_button.setEnabled(false);


//        try {
//            Bitmap map = getBitmapFromAssets("2021-10-06_13.55.20.jpg");
//            Mat mat = new Mat();
//            Utils.bitmapToMat(map, mat);
//            new East(mat);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    private static final Logger LOGGER = new Logger();
    public static final int TF_OD_API_INPUT_SIZE = 320;
    private Button weight_button, temperature_button;
}
