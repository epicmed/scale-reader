package org.tensorflow.lite.examples.detection.utils;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.utils.Tuple;

import java.util.ArrayList;
import java.util.List;

public class utils extends AppCompatActivity {
    private static final Logger LOGGER = new Logger();
    List<Tuple<Tuple<Integer, Integer>, Tuple<Integer, Integer>>> segments ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public utils() {
        segments = new ArrayList<>() ;
    }

    public Bitmap preprocessCNNInput(Bitmap map, RectF location){
        Mat mat = new Mat() ;
        Utils.bitmapToMat(map, mat);
        Imgproc.cvtColor(mat,mat,Imgproc.COLOR_RGB2GRAY);

        Mat roi = new Mat(mat, new Rect(
                (int) location.left,
                (int) location.top,
                (int) (location.right - location.left),
                (int) (location.bottom - location.top)
        ));

        int w = (int) (location.right - location.left);
        int h = (int) (location.bottom - location.top);

        System.out.println(roi.size());

        Core.rotate(roi, roi, Core.ROTATE_90_CLOCKWISE);
        Core.copyMakeBorder(roi,roi,0,0, Math.abs((w-h)/2), Math.abs((w-h)/2), Core.BORDER_CONSTANT, new Scalar(0,0,0));
        Imgproc.resize(roi, roi, new Size(32, 32));

        return convertMatToBitMap(roi);
    }

    public Bitmap cvtGray(Bitmap orig){

        Mat mat = new Mat() ;
        Utils.bitmapToMat(orig, mat);
        Imgproc.cvtColor(mat, mat, Imgproc.COLOR_BGR2GRAY);

        return convertMatToBitMap(mat);
    }

    private static Bitmap convertMatToBitMap(Mat input){
        Bitmap bmp = null;

        try {
            bmp = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(input, bmp);
        }
        catch (CvException e){
            Log.d("Exception",e.getMessage());
        }
        return bmp;
    }

    public double lab_brightness(Mat img) {
        Mat hsv = new Mat() ;
        Imgproc.cvtColor(img, hsv, Imgproc.COLOR_BGR2Lab);

        Imgproc.resize(hsv, hsv, new Size(100, 100)) ;

        List<Mat> split = new ArrayList<>();
        Core.split(hsv, split);
        Core.MinMaxLocResult result = Core.minMaxLoc(split.get(0));

        Mat dst  = new Mat() ;
        Core.divide(split.get(0), new Scalar(result.maxVal), dst);
        double result2 = Core.mean(dst).val[0];
        LOGGER.d("is_low_brightness.dump(): " + result2);

        return result2;
    }
}
