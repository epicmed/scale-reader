package org.tensorflow.lite.examples.detection.utils;

import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.CLAHE;
import org.opencv.imgproc.Imgproc;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.ArrayList;

public class SSOCR {

    double H_W_Ratio = 1.9;
    double THRESHOLD = 35;
    double arc_tan_theta = 6.0;
    int crop_y0 = 215;
    int crop_y1 = 470;
    int crop_x0 = 260;
    int crop_x1 = 890;

    public Mat load_image(String path, boolean show) {
        Mat gray_img = Imgcodecs.imread(path, Imgcodecs.IMREAD_GRAYSCALE);
        double h = gray_img.rows();
        double w = gray_img.cols();

//      # crop_y0 = 0 if h <= crop_y0_init else crop_y0_init
//      # crop_y1 = h if h <= crop_y1_init else crop_y1_init
//      # crop_x0 = 0 if w <= crop_x0_init else crop_x0_init
//      # crop_x1 = w if w <= crop_x1_init else crop_x1_init
//      # gray_img = gray_img[crop_y0:crop_y1, crop_x0:crop_x1]

        Mat blurred = new Mat();
        Imgproc.GaussianBlur(gray_img, blurred, new Size(3,3), 0);

        return blurred;
    }

    public Mat preprocess (Mat img, Mat threshold){
        CLAHE clahe = Imgproc.createCLAHE(2, new Size(6,6));
        clahe.apply(img, img);

        Mat dst = new Mat();
        Imgproc.adaptiveThreshold(img, dst, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.THRESH_BINARY_INV, 127, 50);


        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_CROSS, new Size(5,5));
        Imgproc.morphologyEx(dst, dst, Imgproc.MORPH_CLOSE, kernel);
        Imgproc.morphologyEx(dst, dst, Imgproc.MORPH_OPEN, kernel);

        return dst;
    }

    public void helper_extract(Mat one_d_array, int threshold){
        java.util.List<AbstractMap.SimpleEntry> res = new ArrayList<>() ;
        int flag = 0;
        int temp = 0;

        for(int i = 0; i < one_d_array.cols() ; i++) {
            double[] vals = one_d_array.get(0, i);
            if(vals[0] < 12 * 255) {
                if(flag > threshold) {
                    int start = i - flag;
                    int end = i;
                    temp = end;
                    if(end - start  > 20) {
                        res.add(new AbstractMap.SimpleEntry(start, end)) ;
                    }
                    flag = 0;
                }
                else{
                    flag +=1;
                }
            }
            else{
                if(flag > threshold) {
                    int start = temp;
                    int end = one_d_array.cols();
                    if(end - start > 50) {
                        res.add(new AbstractMap.SimpleEntry(start, end)) ;
                    }
                }
            }
        }
    }


}
