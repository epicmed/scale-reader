package org.tensorflow.lite.examples.detection.unitclassifier;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.examples.detection.SummaryActivity;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.tflite.Classifier;

import java.util.List;

public class unitdec {
    protected TextRecognizer recognizer;
    private static final Logger LOGGER = new Logger();

    public unitdec(){
        recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
    }

    public Task<Text> findAnyUnit(Mat frame) {
        return doUnit(frame);
    }

    private static Bitmap convertMatToBitMap(Mat input){
        Bitmap bmp = null;

        try {
            bmp = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(input, bmp);
        }
        catch (CvException e){
            Log.d("Exception",e.getMessage());
        }
        return bmp;
    }

    private Task<Text> doUnit( Mat roi) {

        Imgproc.cvtColor(roi, roi, Imgproc.COLOR_BGR2GRAY);
        Imgproc.resize(roi, roi, new Size(), 3, 3, Imgproc.INTER_CUBIC);
//        Imgproc.threshold(roi, roi, 127,255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);
//        Mat kernel = new Mat();
//        Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3,3));
//        Mat opening = new Mat();
//        Imgproc.morphologyEx(roi, opening, Imgproc.MORPH_OPEN, kernel, new Point(-1,-1), 1);

        Bitmap b = convertMatToBitMap(roi);
        InputImage img = InputImage.fromBitmap(b, 90);
        Task<Text> task = recognizer.process(img);

        return task;
    }

}
