package org.tensorflow.lite.examples.detection.utils;

import java.io.Serializable;

public class Tuple<K, E> implements Serializable {

    private K k;
    private E e;

    public Tuple(K k, E e) {
        this.k=k;
        this.e=e;
    }

    public E getE() {
        return e;
    }


    public K getK() {
        return k;
    }

}
