package org.tensorflow.lite.examples.detection.decimaldetector;

import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Log;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.examples.detection.tflite.Classifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Decimal {



    public int findDecimal(Bitmap frame, java.util.List<Classifier.Recognition> numbers) {
        RectF r = mergeHelper(numbers);
        Mat mat = new Mat();
        Utils.bitmapToMat(frame, mat);

        int x = (int) r.left;
        int y = (int) r.top;
        int w = (int) r.width();
        int h = (int) r.height();

        return doDecimal(numbers, new Mat(mat, new Rect(
            x,y,w,h
        )), r);
    }

    private RectF mergeHelper(List<Classifier.Recognition> list) {
        if(list.isEmpty()) return new RectF(0,0,0,0) ;

        RectF initial = list.get(0).getLocation() ;

        float left= initial.left,
                top=initial.top,
                right=initial.right,
                bottom=initial.bottom;

        for(Classifier.Recognition o : list) {
            RectF r = o.getLocation();

            if(left > r.left) left = r.left;
            if(top > r.top) top = r.top;
            if(right < r.right) right = r.right;
            if(bottom < r.bottom) bottom = r.bottom;

        }

        return new RectF(left, top, right, bottom) ;
    }

    private Classifier.Recognition findMinimumDistanceToReferencePoint(
            Classifier.Recognition ref,
            List<Classifier.Recognition> list,
            Rect bb) {
        double distRef = 100000;
        Classifier.Recognition best = null;
        for(Classifier.Recognition n : list) {
            RectF l = n.getLocation() ;
//            double dist = Math.sqrt(Math.pow(bb.x - (l.right - ref.getLocation().left), 2) +
//                    Math.pow(bb.y - (l.bottom - ref.getLocation().top), 2));
            double dist = Math.sqrt(Math.pow(bb.x - (l.left ), 2) +
                    Math.pow(bb.y - (l.top ), 2));

            if(distRef > dist) {
                distRef=dist;
                best = n;
            }
        }

        return best;
    }

    private int doDecimal(List<Classifier.Recognition> numbers, Mat roi, RectF rr) {
        Mat lower = new Mat(roi, new Rect(
                (roi.cols()/2),
                0,
                (roi.cols()/2),
                (int) roi.rows()
        ));

        System.out.println("lower: " + lower.cols());
        System.out.println("lower: " + lower.rows());

        Mat gray = new Mat();
        Imgproc.cvtColor(lower, gray, Imgproc.COLOR_RGB2GRAY);
        Mat thresh = new Mat();
        Imgproc.threshold(gray, thresh, 0, 255, Imgproc.THRESH_BINARY + Imgproc.THRESH_OTSU);

        Mat kernel = new Mat();
        Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(2,2));

        Mat opening = new Mat();
        Core.bitwise_not(thresh, opening);
        Imgproc.morphologyEx(opening, opening, Imgproc.MORPH_ERODE, kernel, new Point(-1,-1), 1);
        Imgproc.morphologyEx(opening, opening, Imgproc.MORPH_DILATE, kernel, new Point(-1,-1), 1);

        java.util.List<MatOfPoint> contours = new ArrayList();
        Mat hierarchy = new Mat();
        Imgproc.findContours(opening, contours, hierarchy, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        MatOfPoint bestContour = null;
        float aRatio = 0.0f;
        for(MatOfPoint p : contours){
            double area = Imgproc.contourArea(p);
            Rect r = Imgproc.boundingRect(p);

            if(area < 100) {
                float ratio = r.width / (r.height*1.0f);
                if(ratio > 0.8 && ratio < 1.2 && ratio > aRatio) {
                    aRatio = ratio;
                    bestContour = p;
                }
            }
        }

        if(bestContour != null){
            Rect r = Imgproc.boundingRect(bestContour);
            r.set(
                    new double[]{
                            r.x - (lower.cols()/2) - rr.left,
                            r.y + rr.top,
                            r.width,
                            r.height
                    }
            );

            Classifier.Recognition best = this.findMinimumDistanceToReferencePoint(numbers.get(0), numbers, r);
            return numbers.indexOf(best);
//            Imgproc.rectangle(lower, new Point(r.x, r.y), new Point(r.x+r.width, r.y+r.height), new Scalar(255,0,0), 1);
        }

        return -1;
    }

}
