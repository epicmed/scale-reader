package org.tensorflow.lite.examples.detection.analogue;

//Analogue scale detection (only works for partially visible dial)
//
//        [x] isolate LCD region
//
//        [x] apply stroke width transform
//
//        isolate and identify the numbers
//
//        find text rotation, theta, rotate by 90, 180, 270
//
//        pass numbers through ml kit
//
//        sort numbers in ascending order
//
//        to find needle position, assume lcd is upright and perpendicular to the camera.
//
//        apply probablistic hough line transform,  85degrees < theta < 95degree

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import android.graphics.Bitmap;
import android.graphics.RectF;
import android.util.Log;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.google.mlkit.vision.text.latin.TextRecognizerOptions;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvException;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.tflite.Classifier;
import org.tensorflow.lite.examples.detection.unitclassifier.unitdec;
import org.tensorflow.lite.examples.detection.utils.Tuple;

import java.util.ArrayList;

public class analogue {
    private Classifier classifier;
    private static final Logger LOGGER = new Logger();
    private org.tensorflow.lite.examples.detection.unitclassifier.unitdec unitdec = new unitdec();
    private double needle_pos_x = 0;
    private List<Tuple<Classifier.Recognition, String>> results = new ArrayList<>();

    public analogue(Classifier classifier) {
        this.classifier=classifier;
        recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS);
    }

    private static Bitmap convertMatToBitMap(Mat input){
        Bitmap bmp = null;

        try {
            bmp = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(input, bmp);
        }
        catch (CvException e){
            Log.d("Exception",e.getMessage());
        }
        return bmp;
    }

    public List<Tuple<Classifier.Recognition, String>> doAnalogueDetection2(Bitmap frame, Classifier.Recognition dial_display) {
        Mat mat = new Mat();
        Utils.bitmapToMat(frame, mat);

        RectF r = dial_display.getLocation();

        int x = (int) r.left;
        int y = (int) r.top;
        int w = (int) r.width();
        int h = (int) r.height();

        Mat roi = new Mat(mat, new Rect(
                x,y,w,h
        ));

        Mat roi_2 = roi.clone();

        Imgproc.cvtColor(roi_2, roi_2, Imgproc.COLOR_RGB2GRAY);
        Core.rotate(roi_2, roi_2, Core.ROTATE_90_CLOCKWISE);
        Imgproc.resize(roi_2, roi_2, new Size(416, 416* (roi_2.rows()/(roi_2.cols()*1f))),0,0, Imgproc.INTER_CUBIC);
        Bitmap b = convertMatToBitMap(roi_2);
        final java.util.List<Classifier.Recognition> results =
                this.classifier.recognizeImage(b);

        Mat roi_3 = roi.clone();
        Imgproc.cvtColor(roi_3, roi_3, Imgproc.COLOR_RGB2GRAY, CvType.CV_8UC1);
        Core.rotate(roi_3, roi_3, Core.ROTATE_90_CLOCKWISE);
        Imgproc.resize(roi_3, roi_3, new Size(416, 416* (roi_2.rows()/(roi_2.cols()*1f))),0,0, Imgproc.INTER_CUBIC);
        Imgproc.threshold(roi_3, roi_3, 0, 255, Imgproc.THRESH_BINARY_INV + Imgproc.THRESH_OTSU);

        Collections.sort(results, new Comparator<Classifier.Recognition>() {
            @Override
            public int compare(Classifier.Recognition r1, Classifier.Recognition r2) {
                return (int) (r1.getLocation().left - r2.getLocation().left);
            }
        });

        postProcesing(roi_3, results);
        return this.results;
    }

    private Mat postProcesing(Mat roi , java.util.List<Classifier.Recognition> numbers) {
//        Mat clean = cleanUpImage(roi);
        java.util.List<Tuple<Classifier.Recognition, Mat>> rois = new ArrayList<>() ;
        for(Classifier.Recognition r : numbers) {
            RectF loc = r.getLocation();
            Mat m = new Mat(roi, new Rect(
                    (int) loc.left,
                    (int) loc.top,
                    (int) loc.width(),
                    (int) loc.height()
            ));
            rois.add(new Tuple<>(r, m));
        }

        findNumber(rois);
        return null;
    }

    public MatOfPoint2f col_stack(Mat m){
        Mat mm = m.clone();
        Mat points = Mat.zeros(mm.size(),mm.type());
        Core.findNonZero(mm, points);
        MatOfPoint mpoints = new MatOfPoint(points);
        MatOfPoint2f points2f = new MatOfPoint2f(mpoints.toArray());
        return points2f;
    }

    protected TextRecognizer recognizer;

    public Mat findNumber(java.util.List<Tuple<Classifier.Recognition, Mat>> rois) {

        for(Tuple<Classifier.Recognition, Mat> t: rois) {
            Mat m = t.getE();
            Classifier.Recognition c = t.getK();

            Imgproc.medianBlur(m, m, 5);
            java.util.List<MatOfPoint> contours = new ArrayList<>();
            Mat hierarchy = new Mat();
            Imgproc.findContours(m,contours,hierarchy,Imgproc.RETR_TREE,Imgproc.CHAIN_APPROX_SIMPLE);

            MatOfPoint2f mmm = col_stack(m);
            RotatedRect bb = Imgproc.minAreaRect(mmm);

            double angle =0;
            if(bb.angle < 45)
                angle = ( bb.angle);
            else
                angle = -(90 - bb.angle);

            Mat r_matrix =  Imgproc.getRotationMatrix2D(bb.center, angle, 1);
            Mat roi_r = new Mat();
            Imgproc.warpAffine(m, roi_r, r_matrix, m.size());
            LOGGER.d("analogue: " + bb.angle);

            Core.bitwise_not(m, m);

            Bitmap b = convertMatToBitMap(m);
            InputImage img = InputImage.fromBitmap(b, 0);
            Task<Text> task = recognizer.process(img)
                    .addOnSuccessListener(new OnSuccessListener<Text>() {
                        @Override
                        public void onSuccess(Text t) {
                            LOGGER.d("[recognizer.onSuccess]: " + t.getText() + " - " + c.getLocation());
                            results.add(new Tuple<>(
                                    c,
                                    t.getText().replaceAll("[a-zA-Z]", "0")
                            ));
                        }
                    });
        }

        return null;
    }

    public void findNeedle(Mat roi) {
//        TODO - ASSUME lcd.w / 2
        this.needle_pos_x = roi.cols()/2;
    }

//    public Mat cleanUpImage(Mat roi) {
//        Mat clone = roi.clone();
//        Imgproc.resize(clone, clone, new Size(416, 416* (roi.rows()/(roi.cols()*1f))),0,0, Imgproc.INTER_CUBIC);
//        Imgproc.medianBlur(clone, clone, 5);
//        Imgproc.cvtColor(clone, clone, Imgproc.COLOR_RGB2GRAY, CvType.CV_8SC1);
//        Imgproc.adaptiveThreshold(clone, clone, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
//                Imgproc.THRESH_BINARY, 21, 2);
//        Mat kernel = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5,5), new Point(-1,-1)) ;
//        Core.bitwise_not(clone,clone);
//        Imgproc.morphologyEx(clone, clone, Imgproc.MORPH_OPEN, kernel, new Point(-1,-1), 1);
//        Core.bitwise_not(clone,clone);
//        return clone;
//    }
}
