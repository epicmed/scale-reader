
package org.tensorflow.lite.examples.detection;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.vision.text.Text;
import org.opencv.android.Utils;
import org.opencv.core.CvException;
import org.opencv.core.Mat;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.unitclassifier.unitdec;


public class SummaryActivity extends AppCompatActivity {
    private Button accept, reject;
    private static final Logger LOGGER = new Logger();
    private TextView weightTextView, error, bmi;
    private ImageView lcdImage;
    private String type ;
    private org.tensorflow.lite.examples.detection.unitclassifier.unitdec unitdec = new unitdec();
    private String[] findWhat = new String[]{};

    private static Bitmap convertMatToBitMap(Mat input){
        Bitmap bmp = null;

        try {
            bmp = Bitmap.createBitmap(input.cols(), input.rows(), Bitmap.Config.ARGB_8888);
            Utils.matToBitmap(input, bmp);
        }
        catch (CvException e){
            Log.d("Exception",e.getMessage());
        }
        return bmp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tfe_od_results);
        weightTextView = findViewById(R.id.weight_info_text_input);
        lcdImage = findViewById(R.id.bitmap_lcd);
        accept = findViewById(R.id.back);
        bmi = findViewById(R.id.bmi_val);
        reject = findViewById(R.id.reject);
        error = findViewById(R.id.error_text);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivities();
            }
        });
        reject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switchActivities2();
            }
        });


        Spinner spinner = (Spinner) findViewById(R.id.units);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.units_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

//        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
//                // your code here
//                if(position == 0) {
//                    bmi.setText(
//                            Float.toString((float) (Float.parseFloat(weightTextView.getText().toString()) / Math.pow(1.752, 2)))
//                    );
//                }
//                else{
//                    bmi.setText(
//                            Float.toString((float) ((Float.parseFloat(weightTextView.getText().toString())/2.2046) / Math.pow(1.752, 2)))
//                    );
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> parentView) {
//                // your code here
//            }
//
//        });

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            String data = extras.getString("data"); // retrieve the data using keyName
            String type = extras.getString("type"); // retrieve the data using keyName
            this.type=type;

//            long addr = getIntent().getLongExtra("addr", 0);
//            Mat tempImg = new Mat( addr );
//            lcdImage.setImageBitmap(convertMatToBitMap(tempImg));
//            this.findWhat = "kg,lb".split(",");
//            unitdec.findAnyUnit(tempImg)
//                    .addOnSuccessListener(new OnSuccessListener<Text>() {
//                        @Override
//                        public void onSuccess(Text visionText) {
//                            LOGGER.d("[recognizer.onSuccess]: " + visionText.getText()) ;
//                            String[] split = visionText.getText().split("\n");
//
//                            boolean found = false;
//                            for(String s : split) {
//                                s = s.toLowerCase().trim().replaceAll(" ", "");
//                                for(String k : findWhat){
//                                    if(s.equals(k)) {
//                                        spinner.setSelection(
//                                                s.equals("kg") ? 0 : 1
//                                        );
//                                        found = true;
//                                        break;
//                                    }
//                                }
//                            }
//
//                            if(!found) {
//                                error.setText("Could not find units. Default unit: KG");
//
//                                bmi.setText(
//                                        Float.toString((float) (Float.parseFloat(data) / Math.pow(1.752, 2)))
//                                );
//                            }
//                            else{
//                                error.setText("Found units.");
//
//                                if(spinner.getSelectedItemPosition() == 0) {
//                                    bmi.setText(
//                                            Float.toString((float) (Float.parseFloat(data) / Math.pow(1.752, 2)))
//                                    );
//                                }
//                                else{
//                                    bmi.setText(
//                                            Float.toString((float) ((Float.parseFloat(data)/2.2046) / Math.pow(1.752, 2)))
//                                    );
//                                }
//
//                            }
//
//                        }
//                    })
//                    .addOnFailureListener(new OnFailureListener() {
//                        @Override
//                        public void onFailure(@NonNull Exception e) {
//                            LOGGER.d("[recognizer.onFailure]: " + e.getMessage()) ;
//                            error.setText("Could not find units. Please manually select units from dropdown.");
//                        }
//                    });

            weightTextView.setText(data);

        }
    }

    private void switchActivities() {
        Intent switchActivityIntent = new Intent(this, MainActivity.class);
        switchActivityIntent.putExtra("type", this.type);
        startActivity(switchActivityIntent);
    }

    private void switchActivities2() {
        Intent switchActivityIntent = new Intent(this, DetectorActivity.class);
        switchActivityIntent.putExtra("type", this.type);
        startActivity(switchActivityIntent);
    }

}



